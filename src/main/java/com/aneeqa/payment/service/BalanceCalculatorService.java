package com.aneeqa.payment.service;

import com.aneeqa.payment.model.InputModel;
import com.aneeqa.payment.model.OutputModel;
import com.aneeqa.payment.model.TransactionModel;
import com.aneeqa.payment.model.TransactionType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.ObjectUtils;

public class BalanceCalculatorService {
	public OutputModel calculateBalance(InputModel inputModel, List<TransactionModel> transactions) {
		String accountId = inputModel.getAccountId();
		BigDecimal balance = BigDecimal.ZERO;
		int count = 0;
		if (ObjectUtils.isNotEmpty(transactions)) {
			List<TransactionModel> accountTransactions = transactions.stream()
																	 .filter(transaction -> transaction.getToAccountId()
																									   .equals(accountId)
																		 || transaction.getFromAccountId()
																					   .equals(accountId))
																	 .collect(Collectors.toList());
			List<TransactionModel> paymentTransactions = new ArrayList<>();
			List<TransactionModel> reversalTransactions = new ArrayList<>();

			for (TransactionModel transaction : accountTransactions) {
				if (transaction.getTransactionType() == TransactionType.PAYMENT && transaction.getCreatedAt()
																							  .isAfter(inputModel.getFromDate())
					&& transaction.getCreatedAt()
								  .isBefore(inputModel.getToDate())) {
					paymentTransactions.add(transaction);
				} else if (transaction.getTransactionType() == TransactionType.REVERSAL) {
					reversalTransactions.add(transaction);
				}
			}

			for (TransactionModel transaction : paymentTransactions) {
				boolean hasReversal = reversalTransactions.stream()
														  .filter(reversalTransaction -> reversalTransaction != null
															  && reversalTransaction.getRelatedTransactionId() != null
															  && reversalTransaction.getRelatedTransactionId()
																					.equals(transaction.getTransactionId()))
														  .findAny()
														  .isPresent();
				if (!hasReversal) {
					if (transaction.getFromAccountId()
								   .equals(inputModel.getAccountId())) {
						balance = balance.subtract(transaction.getAmount());
					} else {
						balance = balance.add(transaction.getAmount());
					}
					count++;
				}
			}
		}
		return new OutputModel(balance, count);
	}
}
