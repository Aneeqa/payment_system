package com.aneeqa.payment.model;

public class PaymentConstants {
	public static final String dateFormat = "dd/MM/yyyy HH:mm:ss";
	public static final String inputFile = "input.csv";
}
