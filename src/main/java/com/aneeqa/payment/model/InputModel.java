package com.aneeqa.payment.model;

import java.time.LocalDateTime;

public class InputModel {

	private String accountId;

	private LocalDateTime fromDate;

	private LocalDateTime toDate;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public LocalDateTime getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDateTime fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDateTime getToDate() {
		return toDate;
	}

	public void setToDate(LocalDateTime toDate) {
		this.toDate = toDate;
	}

	public InputModel() {
	}

	public InputModel(String accountId, LocalDateTime fromDate, LocalDateTime toDate) {
		this.accountId = accountId;
		this.fromDate = fromDate;
		this.toDate = toDate;
	}
}
