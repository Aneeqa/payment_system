import static org.junit.Assert.assertEquals;

import com.aneeqa.payment.model.InputModel;
import com.aneeqa.payment.model.OutputModel;
import com.aneeqa.payment.model.PaymentConstants;
import com.aneeqa.payment.model.TransactionModel;
import com.aneeqa.payment.model.TransactionType;
import com.aneeqa.payment.service.BalanceCalculatorService;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

public class BalanceCalculatorServiceTest {

	private BalanceCalculatorService balanceCalculatorService;
	private InputModel inputModel = new InputModel();
	private DateTimeFormatter formatter;

	@Before
	public void init() {
		balanceCalculatorService = new BalanceCalculatorService();

		formatter = DateTimeFormatter.ofPattern(PaymentConstants.dateFormat);
		inputModel.setAccountId("A123");
		inputModel.setFromDate(LocalDateTime.parse("20/10/2020 12:00:00", formatter));
		inputModel.setToDate(LocalDateTime.parse("20/10/2020 19:00:00", formatter));

	}

	@Test
	public void testCalculateBalance_NullTransactions() {
		OutputModel outputModel = balanceCalculatorService.calculateBalance(inputModel, null);
		assertEquals(BigDecimal.ZERO, outputModel.getRelativeBalance());
		assertEquals(0, outputModel.getNumberOfTransactions());
	}

	@Test
	public void testCalculateBalance_EmptyList() {
		List<TransactionModel> transactions = new ArrayList<>();
		OutputModel outputModel = balanceCalculatorService.calculateBalance(inputModel, transactions);
		assertEquals(BigDecimal.ZERO, outputModel.getRelativeBalance());
		assertEquals(0, outputModel.getNumberOfTransactions());
	}

	@Test
	public void testCalculateBalance_OnePaymentFromAccount() {
		List<TransactionModel> transactions = new ArrayList<>();

		TransactionModel transaction = new TransactionModel();
		transaction.setTransactionId("TX123");
		transaction.setFromAccountId("A123");
		transaction.setToAccountId("A456");
		transaction.setCreatedAt(LocalDateTime.parse("20/10/2020 13:00:00", formatter));
		transaction.setAmount(BigDecimal.valueOf(100));
		transaction.setTransactionType(TransactionType.PAYMENT);

		transactions.add(transaction);

		OutputModel outputModel = balanceCalculatorService.calculateBalance(inputModel, transactions);
		assertEquals(BigDecimal.valueOf(-100), outputModel.getRelativeBalance());
		assertEquals(1, outputModel.getNumberOfTransactions());
	}

	@Test
	public void testCalculateBalance_OnePaymentToAccount() {
		List<TransactionModel> transactions = new ArrayList<>();

		TransactionModel transaction = new TransactionModel();
		transaction.setTransactionId("TX123");
		transaction.setFromAccountId("A456");
		transaction.setToAccountId("A123");
		transaction.setCreatedAt(LocalDateTime.parse("20/10/2020 13:00:00", formatter));
		transaction.setAmount(BigDecimal.valueOf(100));
		transaction.setTransactionType(TransactionType.PAYMENT);

		transactions.add(transaction);

		OutputModel outputModel = balanceCalculatorService.calculateBalance(inputModel, transactions);
		assertEquals(BigDecimal.valueOf(100), outputModel.getRelativeBalance());
		assertEquals(1, outputModel.getNumberOfTransactions());
	}

	@Test
	public void testCalculateBalance_OnePaymentToAccountWithReversal() {
		List<TransactionModel> transactions = new ArrayList<>();

		TransactionModel transaction1 = new TransactionModel();
		transaction1.setTransactionId("TX123");
		transaction1.setFromAccountId("A456");
		transaction1.setToAccountId("A123");
		transaction1.setCreatedAt(LocalDateTime.parse("20/10/2020 13:00:00", formatter));
		transaction1.setAmount(BigDecimal.valueOf(100));
		transaction1.setTransactionType(TransactionType.PAYMENT);

		TransactionModel transaction2 = new TransactionModel();
		transaction2.setTransactionId("TX234");
		transaction2.setFromAccountId("A456");
		transaction2.setToAccountId("A123");
		transaction2.setCreatedAt(LocalDateTime.parse("20/10/2020 15:00:00", formatter));
		transaction2.setAmount(BigDecimal.valueOf(100));
		transaction2.setRelatedTransactionId("TX123");
		transaction2.setTransactionType(TransactionType.REVERSAL);

		transactions.add(transaction1);
		transactions.add(transaction2);

		OutputModel outputModel = balanceCalculatorService.calculateBalance(inputModel, transactions);
		assertEquals(BigDecimal.valueOf(0), outputModel.getRelativeBalance());
		assertEquals(0, outputModel.getNumberOfTransactions());
	}

	@Test
	public void testCalculateBalance_TwoPaymentToAccountWithReversal() {
		List<TransactionModel> transactions = new ArrayList<>();

		TransactionModel transaction1 = new TransactionModel();
		transaction1.setTransactionId("TX123");
		transaction1.setFromAccountId("A456");
		transaction1.setToAccountId("A123");
		transaction1.setCreatedAt(LocalDateTime.parse("20/10/2020 13:00:00", formatter));
		transaction1.setAmount(BigDecimal.valueOf(100));
		transaction1.setTransactionType(TransactionType.PAYMENT);

		TransactionModel transaction2 = new TransactionModel();
		transaction2.setTransactionId("TX234");
		transaction2.setFromAccountId("A456");
		transaction2.setToAccountId("A123");
		transaction2.setCreatedAt(LocalDateTime.parse("20/10/2020 13:00:00", formatter));
		transaction2.setAmount(BigDecimal.valueOf(100));
		transaction2.setTransactionType(TransactionType.PAYMENT);

		TransactionModel transaction3 = new TransactionModel();
		transaction3.setTransactionId("TX456");
		transaction3.setFromAccountId("A456");
		transaction3.setToAccountId("A123");
		transaction3.setCreatedAt(LocalDateTime.parse("20/10/2020 15:00:00", formatter));
		transaction3.setAmount(BigDecimal.valueOf(100));
		transaction3.setRelatedTransactionId("TX123");
		transaction3.setTransactionType(TransactionType.REVERSAL);

		transactions.add(transaction1);
		transactions.add(transaction2);
		transactions.add(transaction3);

		OutputModel outputModel = balanceCalculatorService.calculateBalance(inputModel, transactions);
		assertEquals(BigDecimal.valueOf(100), outputModel.getRelativeBalance());
		assertEquals(1, outputModel.getNumberOfTransactions());
	}
}
