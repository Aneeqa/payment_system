package com.aneeqa.payment.model;

import java.math.BigDecimal;

public class OutputModel {

	private BigDecimal relativeBalance;

	private int numberOfTransactions;

	public OutputModel() {
	}

	public OutputModel(BigDecimal relativeBalance, int numberOfTransactions) {
		this.relativeBalance = relativeBalance;
		this.numberOfTransactions = numberOfTransactions;
	}

	public BigDecimal getRelativeBalance() {
		return relativeBalance;
	}

	public void setRelativeBalance(BigDecimal relativeBalance) {
		this.relativeBalance = relativeBalance;
	}

	public int getNumberOfTransactions() {
		return numberOfTransactions;
	}

	public void setNumberOfTransactions(int numberOfTransactions) {
		this.numberOfTransactions = numberOfTransactions;
	}
}
