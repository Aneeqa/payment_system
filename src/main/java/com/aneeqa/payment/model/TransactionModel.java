package com.aneeqa.payment.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvDate;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class TransactionModel {

	@CsvBindByName(column = "transactionId")
	private String transactionId;

	@CsvBindByName(column = "fromAccountId")
	private String fromAccountId;

	@CsvBindByName(column = "toAccountId")
	private String toAccountId;

	@CsvDate(value = PaymentConstants.dateFormat)
	@CsvBindByName(column = "createdAt")
	private LocalDateTime createdAt;

	@CsvBindByName(column = "amount")
	private BigDecimal amount;

	@CsvBindByName(column = "transactionType")
	private TransactionType transactionType;

	@CsvBindByName(column = "relatedTransaction")
	private String relatedTransactionId;

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getFromAccountId() {
		return fromAccountId;
	}

	public void setFromAccountId(String fromAccountId) {
		this.fromAccountId = fromAccountId;
	}

	public String getToAccountId() {
		return toAccountId;
	}

	public void setToAccountId(String toAccountId) {
		this.toAccountId = toAccountId;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public String getRelatedTransactionId() {
		return relatedTransactionId;
	}

	public void setRelatedTransactionId(String relatedTransactionId) {
		this.relatedTransactionId = relatedTransactionId;
	}

	public TransactionModel() {
	}

	public TransactionModel(String transactionId, String fromAccountId, String toAccountId, LocalDateTime createdAt,
		BigDecimal amount, TransactionType transactionType, String relatedTransactionId) {
		this.transactionId = transactionId;
		this.fromAccountId = fromAccountId;
		this.toAccountId = toAccountId;
		this.createdAt = createdAt;
		this.amount = amount;
		this.transactionType = transactionType;
		this.relatedTransactionId = relatedTransactionId;
	}

	@Override
	public String toString() {
		return "TransactionModel{" + "transactionId='" + transactionId + '\'' + ", fromAccountId='" + fromAccountId
			+ '\'' + ", toAccountId='" + toAccountId + '\'' + ", createdAt=" + createdAt + ", amount=" + amount
			+ ", transactionType=" + transactionType + ", relatedTransactionId='" + relatedTransactionId + '\'' + '}';
	}
}
