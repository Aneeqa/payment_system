package util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import com.aneeqa.payment.model.TransactionModel;
import com.aneeqa.payment.util.FileUtil;
import java.util.List;
import org.junit.Test;

public class FileUtilTest {

	@Test
	public void testReadFile() throws Exception {
		FileUtil fileUtil = new FileUtil();
		List<TransactionModel> transactions = fileUtil.readFile("input.csv");
		assertEquals(5, transactions.size());
	}

	@Test
	public void testReadFile_BlankFileName() {
		FileUtil fileUtil = new FileUtil();
		try {
			fileUtil.readFile("");
			fail("Should throw exception");
		} catch (Exception e) {
			assertEquals("Empty file path.", e.getMessage());
		}
	}

	@Test
	public void testReadFile_NullFileName() {
		FileUtil fileUtil = new FileUtil();
		try {
			fileUtil.readFile(null);
			fail("Should throw exception");
		} catch (Exception e) {
			assertEquals("Empty file path.", e.getMessage());
		}
	}

	@Test
	public void testReadFile_NonExistingFile() {
		FileUtil fileUtil = new FileUtil();
		try {
			fileUtil.readFile("xyz.csv");
			fail("Should throw exception");
		} catch (Exception e) {
			assertEquals("Couldn't find file xyz.csv", e.getMessage());
		}
	}
}
