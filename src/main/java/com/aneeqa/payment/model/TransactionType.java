package com.aneeqa.payment.model;

public enum TransactionType {
	PAYMENT,
	REVERSAL
}
