package com.aneeqa.payment;

import com.aneeqa.payment.model.InputModel;
import com.aneeqa.payment.model.OutputModel;
import com.aneeqa.payment.model.PaymentConstants;
import com.aneeqa.payment.model.TransactionModel;
import com.aneeqa.payment.service.BalanceCalculatorService;
import com.aneeqa.payment.util.FileUtil;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

public class ProcessPayments {

	public static void main(String args[]) throws Exception {

		//Read csv
		FileUtil fileUtil = new FileUtil();
		List<TransactionModel> transactions = fileUtil.readFile(PaymentConstants.inputFile);

		//Read input
		ProcessPayments processPayments = new ProcessPayments();
		InputModel inputModel = processPayments.getInput();

		//process
		BalanceCalculatorService balanceCalculatorService = new BalanceCalculatorService();
		OutputModel output = balanceCalculatorService.calculateBalance(inputModel, transactions);

		//Display Output
		NumberFormat currency = NumberFormat.getCurrencyInstance();
		String myCurrency = currency.format(output.getRelativeBalance());
		System.out.println("\nRelative balance for the period is: " + myCurrency);
		System.out.println("Number of transactions included is: " + output.getNumberOfTransactions());

	}

	public InputModel getInput() {

		InputModel inputModel = new InputModel();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PaymentConstants.dateFormat);

		System.out.print("accountId: ");
		Scanner input = new Scanner(System.in);
		inputModel.setAccountId(input.nextLine().trim());

		System.out.print("\nfrom: ");
		String selectedFromDateStr = input.nextLine().trim();
		inputModel.setFromDate(LocalDateTime.parse(selectedFromDateStr, formatter));

		System.out.print("\nto: ");
		String selectedToDateStr = input.nextLine().trim();
		inputModel.setToDate(LocalDateTime.parse(selectedToDateStr, formatter));

		return inputModel;
	}
}
