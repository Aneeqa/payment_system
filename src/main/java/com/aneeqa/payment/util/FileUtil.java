package com.aneeqa.payment.util;

import com.aneeqa.payment.model.TransactionModel;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.enums.CSVReaderNullFieldIndicator;
import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public class FileUtil {

    public List<TransactionModel> readFile(String filePath) throws Exception {

        if (StringUtils.isEmpty(filePath)) {
            throw new Exception("Empty file path.");
        }

        URL url = ClassLoader.getSystemResource(filePath);
        Reader reader;
        List<TransactionModel> transactions;
        if (url != null) {
            reader = Files.newBufferedReader(Paths.get(url.toURI()));
            transactions = new CsvToBeanBuilder(reader).withType(TransactionModel.class)
                                            .withIgnoreLeadingWhiteSpace(true)
                                            .withFieldAsNull(CSVReaderNullFieldIndicator.EMPTY_SEPARATORS)
                                            .build()
                                            .parse();
        } else {
            throw new Exception("Couldn't find file " + filePath);
        }

        if (reader != null) {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return transactions;
    }
    
}
